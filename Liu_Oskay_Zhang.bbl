\begin{thebibliography}{30}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Kirane and Ghosh(2008)]{Kirane2008}
Kedar Kirane and Somnath Ghosh.
\newblock A cold dwell fatigue crack nucleation criterion for polycrystalline
  ti-6242 using grain-level crystal plasticity fe model.
\newblock \emph{Int. J. Fatigue}, 30:\penalty0 2127 -- 2139, 2008.

\bibitem[Anahid and Ghosh(2013)]{Anahid2013}
Masoud Anahid and Somnath Ghosh.
\newblock Homogenized constitutive and fatigue nucleation models from crystal
  plasticity fe simulations of ti alloys, part 2: macroscopic probabilistic
  crack nucleation model.
\newblock \emph{Int. J. Plast.}, 48:\penalty0 111--124, 2013.

\bibitem[Ozturk et~al.(2017)Ozturk, Pilchak, and Ghosh]{Ozturk2017}
D.~Ozturk, A.L. Pilchak, and S.~Ghosh.
\newblock Experimentally validated dwell and cyclic fatigue crack nucleation
  model for α--titanium alloys.
\newblock \emph{Scr. Mater.}, 127:\penalty0 15 -- 18, 2017.

\bibitem[Sinha et~al.(2006)Sinha, Mills, and Williams]{Sinha2006}
V.~Sinha, M.~J. Mills, and J.~C. Williams.
\newblock Crystallography of fracture facets in a near-alpha titanium alloy.
\newblock \emph{Metall. Mater. Trans. A}, 37:\penalty0 2015, 2006.

\bibitem[Pilchak and Williams(2011)]{Pilchak2011}
A.~L. Pilchak and J.~C. Williams.
\newblock Observations of facet formation in near-α titanium and comments on
  the role of hydrogen.
\newblock \emph{Metall. Mater. Trans. A}, 42:\penalty0 1000, 2011.

\bibitem[Pilchak et~al.(2014)Pilchak, Li, and Rokhlin]{Pilchak2014}
Adam~L. Pilchak, Jia Li, and Stanislav~I. Rokhlin.
\newblock Quantitative comparison of microtexture in near-alpha titanium
  measured by ultrasonic scattering and electron backscatter diffraction.
\newblock \emph{Metall. Mater. Trans. A}, 45:\penalty0 4679--4697, 2014.

\bibitem[Sinha et~al.(2017)Sinha, Jha, Pilchak, Porter, John, and
  Larsen]{Sinha2017}
V.~Sinha, S.~K. Jha, A.~L. Pilchak, W.~J. Porter, R.~John, and J.~M. Larsen.
\newblock Quantitative characterization of microscale fracture features in
  titanium alloys.
\newblock \emph{Metall. Mater. Trans. A}, 6:\penalty0 261--269, 2017.

\bibitem[Kartal et~al.(2014)Kartal, Cuddihy, and Dunne]{Kartal2014}
ME~Kartal, MA~Cuddihy, and FPE Dunne.
\newblock Effects of crystallographic orientation and grain morphology on crack
  tip stress state and plasticity.
\newblock \emph{Int. J. Fatigue}, 61:\penalty0 46--58, 2014.

\bibitem[Kirane et~al.(2009)Kirane, Ghosh, Groeber, and
  Bhattacharjee]{Kirane2009}
Kedar Kirane, Somnath Ghosh, Mike Groeber, and Amit Bhattacharjee.
\newblock Grain level dwell fatigue crack nucleation model for ti alloys using
  crystal plasticity finite element analysis.
\newblock \emph{J. Eng. Mater. Technol}, 131:\penalty0 021003, 2009.

\bibitem[Przybyla and McDowell(2011)]{Przybyla2011}
Craig~P. Przybyla and David~L. McDowell.
\newblock Simulated microstructure-sensitive extreme value probabilities for
  high cycle fatigue of duplex ti--6al--4v.
\newblock \emph{Int. J. Plast.}, 27:\penalty0 1871 -- 1895, 2011.

\bibitem[Anahid et~al.(2011)Anahid, Samal, and Ghosh]{Anahid2011}
Masoud Anahid, Mahendra~K Samal, and Somnath Ghosh.
\newblock Dwell fatigue crack nucleation model based on crystal plasticity
  finite element simulations of polycrystalline titanium alloys.
\newblock \emph{J. Mech. Phys. Solids.}, 59:\penalty0 2157--2176, 2011.

\bibitem[Dunne et~al.(2007{\natexlab{a}})Dunne, Wilkinson, and
  Allen]{Dunne2007b}
F.P.E. Dunne, A.J. Wilkinson, and R.~Allen.
\newblock Experimental and computational studies of low cycle fatigue crack
  nucleation in a polycrystal.
\newblock \emph{Int. J. Plast.}, 23\penalty0 (2):\penalty0 273 -- 295,
  2007{\natexlab{a}}.

\bibitem[McDowell(2007)]{Mcdowell2007}
David~L. McDowell.
\newblock Simulation-based strategies for microstructure-sensitive fatigue
  modeling.
\newblock \emph{Materials Science and Engineering: A}, 468-470:\penalty0 4 --
  14, 2007.

\bibitem[McDowell and Dunne(2010)]{McDowell2010}
D.L. McDowell and F.P.E. Dunne.
\newblock Microstructure-sensitive computational modeling of fatigue crack
  formation.
\newblock \emph{Int. J. Fatigue}, 32:\penalty0 1521 -- 1542, 2010.

\bibitem[Liu et~al.(2018{\natexlab{a}})Liu, Zhang, Zhu, Hu, and Oskay]{Liu2018}
Yang Liu, Xiang Zhang, Yiguo Zhu, Ping Hu, and Caglar Oskay.
\newblock Multiscale prediction of fatigue crack initiation sites in structures
  made of titanium alloys.
\newblock \emph{Int. J. Plast.(Submitted)}, 2018{\natexlab{a}}.

\bibitem[Zhang and Oskay(2015)]{Zhang2015}
Xiang Zhang and Caglar Oskay.
\newblock Eigenstrain based reduced order homogenization for polycrystalline
  materials.
\newblock \emph{Comput. Meth. Appl. Mech. Engrg.}, 297:\penalty0 408--436,
  2015.

\bibitem[Zhang and Oskay(2017)]{Zhang2017}
Xiang Zhang and Caglar Oskay.
\newblock Sparse and scalable eigenstrain-based reduced order homogenization
  models for polycrystal plasticity.
\newblock \emph{Comput. Meth. Appl. Mech. Engrg.}, 326:\penalty0 241--269,
  2017.

\bibitem[Dvorak(1992)]{Dvorak:1992a}
G.~J. Dvorak.
\newblock Transformation field analysis of inelastic composite materials.
\newblock \emph{Proceedings: Mathematical and Physical Sciences}, 437:\penalty0
  311--327, 1992.

\bibitem[Ardeljan et~al.(2014)Ardeljan, Beyerlein, and Knezevic]{Ardeljan2014}
Milan Ardeljan, Irene~J Beyerlein, and Marko Knezevic.
\newblock A dislocation density based crystal plasticity finite element model:
  application to a two-phase polycrystalline hcp/bcc composites.
\newblock \emph{J. Mech. Phys. Solids.}, 66:\penalty0 16--31, 2014.

\bibitem[Knezevic et~al.(2013)Knezevic, Lebensohn, Cazacu, Revil-Baudard,
  Proust, Vogel, and Nixon]{Knezevic2013a}
Marko Knezevic, Ricardo~A Lebensohn, Oana Cazacu, Benoit Revil-Baudard,
  Gw{\'e}na{\"e}lle Proust, Sven~C Vogel, and Michael~E Nixon.
\newblock Modeling bending of $\alpha$-titanium with embedded polycrystal
  plasticity in implicit finite elements.
\newblock \emph{Mater. Sci. Eng. A}, 564:\penalty0 116--126, 2013.

\bibitem[Fatemi and Socie(1988)]{Fatemi1988}
Ali Fatemi and Darrell~F. Socie.
\newblock A critical plane approach to multiaxial fatigue damage including
  out-of-phase loading.
\newblock \emph{Fatigue Fract. Eng. Mater. Struct.}, 11\penalty0 (3):\penalty0
  149--165, 1988.

\bibitem[McDowell(2005)]{McDowell2005}
DL~McDowell.
\newblock \emph{Microstructure-Sensitive Computational Fatigue Analysis. In:
  Yip S. (eds) Handbook of Materials Modeling}.
\newblock Springer, 2005.

\bibitem[Dunne et~al.(2007{\natexlab{b}})Dunne, Rugg, and Walker]{Dunne2007}
FPE Dunne, D~Rugg, and A~Walker.
\newblock Lengthscale-dependent, elastically anisotropic, physically-based hcp
  crystal plasticity: application to cold-dwell fatigue in ti alloys.
\newblock \emph{Int. J. Plast.}, 23:\penalty0 1061--1083, 2007{\natexlab{b}}.

\bibitem[Zhang and Oskay(2016)]{Zhang2016b}
X.~Zhang and C.~Oskay.
\newblock Polycrystal plasticity modeling of nickel-based superalloy in 617
  subjected to cyclic loading at high temperature.
\newblock \emph{Modelling Simul. Mater. Sci. Eng.}, 24:\penalty0 055009, 2016.

\bibitem[Phan et~al.(2017)Phan, Zhang, Li, and Oskay]{Phan2017}
V.T. Phan, X.~Zhang, Y.~Li, and C.~Oskay.
\newblock Microscale modeling of creep deformation and rupture in nickel-based
  superalloy in 617 at high temperature.
\newblock \emph{Mech. Mater.}, 114:\penalty0 215--227, 2017.

\bibitem[Hu et~al.(2016)Hu, Liu, Zhu, and Ying]{Hu2016}
Ping Hu, Yang Liu, Yiguo Zhu, and Liang Ying.
\newblock Crystal plasticity extended models based on thermal mechanism and
  damage functions: Application to multiscale modeling of aluminum alloy
  tensile behavior.
\newblock \emph{Int. J. Plast.}, 86:\penalty0 1 -- 25, 2016.

\bibitem[Liu et~al.(2018{\natexlab{b}})Liu, Zhu, Oskay, Hu, Ying, and
  Wang]{Liu2018b}
Yang Liu, Yiguo Zhu, Caglar Oskay, Ping Hu, Liang Ying, and Dantong Wang.
\newblock Experimental and computational study of microstructural effect on
  ductile fracture of hot-forming materials.
\newblock \emph{Mater. Sci. Eng. A}, 724:\penalty0 298--323,
  2018{\natexlab{b}}.

\bibitem[Gockel et~al.(2016)Gockel, Kolesar, and Rollett]{Gockel2016}
Brian~T Gockel, Ryan~S Kolesar, and Anthony~D Rollett.
\newblock Experimental study of an aerospace titanium alloy under various
  thermal and tensile loading rate conditions.
\newblock \emph{Integr. Mater. Manuf. Innov.}, 5:\penalty0 13, 2016.

\bibitem[Pilchak(2013)]{Pilchak2013}
Adam~L Pilchak.
\newblock Fatigue crack growth rates in alpha titanium: faceted vs. striation
  growth.
\newblock \emph{Scr. Mater.}, 68:\penalty0 277--280, 2013.

\bibitem[Pilchak et~al.(2016)Pilchak, Shank, Tucker, Srivatsa, Fagin, and
  Semiatin]{Pilchak2016}
Adam~L Pilchak, Jared Shank, Joseph~C Tucker, Shesh Srivatsa, Patrick~N Fagin,
  and S~Lee Semiatin.
\newblock A dataset for the development, verification, and validation of
  microstructure-sensitive process models for near-alpha titanium alloys.
\newblock \emph{Integr. Mater. Manuf. Innov.}, 5:\penalty0 14, 2016.

\end{thebibliography}
